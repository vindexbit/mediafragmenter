BS=build-scripts
PREFIX=/usr/local
DESTDIR=

DEFAULT_PLANG=D
PLANG=$(DEFAULT_PLANG)
MULTILINGUAL=FALSE
ifeq ($(MULTILINGUAL),TRUE)
	PLANG_DIRNAME=lang_$(PLANG)/
else
	PLANG_DIRNAME=
endif

ifeq ($(PLANG),D)
	DC=ldc2
	PHOBOS_LINKING=dynamic
	AMALTHEA_LINKING=dynamic
	RELEASE_ARGS=release $(DC) $(PHOBOS_LINKING) $(AMALTHEA_LINKING)
	DEBUG_ARGS=debug $(DC) $(PHOBOS_LINKING) $(AMALTHEA_LINKING)
endif

bin:
	$(BS)/$(PLANG_DIRNAME)compile.sh $(RELEASE_ARGS)

debug:
	$(BS)/$(PLANG_DIRNAME)compile.sh $(DEBUG_ARGS)

test:
	$(BS)/run_tests.sh

install:
	$(BS)/install.sh --install $(DESTDIR)$(PREFIX)

uninstall:
	$(BS)/install.sh --uninstall $(DESTDIR)$(PREFIX)

tarball:
	$(BS)/build_tarball.sh

clean:
	rm -rf build/

