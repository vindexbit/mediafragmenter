This project is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

---

## About

This project provides two programs: mediafragmenter and fragpointer.

**mediafragmenter** is a tool for creating pointers to fragments.

**fragplayer** is a tool for using pointers to fragments.

*A pointer to a fragment* is a file with a description of the file type, the file path, the begin of the fragment in the file and the end of the fragment.

In current version, working with fragments of video, audio and plain text documents
(txt, xml, source code, etc.) is available.

---

## Ready-made packages

See [download page](https://gitlab.com/vindexbit/mediafragmenter/wikis/Download-page).

---

## Build from source

### Preparing

Before assembling, you need to install a compiler for D (this project supports compiler [ldc](https://github.com/ldc-developers/ldc/) only) and [chrpath](https://directory.fsf.org/wiki/Chrpath).

For example, in Debian-based distributions, you can install required packages as follows:

`sudo apt install ldc chrpath`

Similarly, in Fedora:

`sudo dnf install ldc chrpath`

This project is assembled with a static linking to the [Amalthea library](https://gitlab.com/vindexbit/amalthea). So if you want to build this project from source, you also need to build and install Amalthea. Then return to this instruction.


### Compilation and installation

Creating of executable bin-files of mediafragmenter and fragplayer:

`make`


Installation:

`sudo make install`

After that, all is ready for use.

Uninstall:

`sudo make uninstall`

---

## Usage

This section is written based on materials from the help files.

Actual help is available when using the commands:

`>: mediafragmenter --help`

`>: fragplayer --help`

### mediafragmenter

`mediafragmenter <file> [-t <type>] [-s <begin>] [-f <end>] [-o <output file>]`

Only the path to the file is required, the pointer to the fragment of which is created; other parameters are optional.

The -t (--type) flag allows you to specify the file type: video, audio or text.

If the flag is not specified, the type is determined automatically.

The -s (--start) and -f (--finish) flags specify the boundaries of the fragment:

- for audio and video types, the time stamps are used in the format HH:MM:SS (hours, minutes, seconds with a colons); seconds can have a fractional part up to four characters after point;

- for text, the boundaries are the row number and the column number (number of symbol in the row) separated by comma (without a space); the boundaries are closed, i.e. fragments includes specified boundaries;

- the absence of the value of the begin and/or end of the fragment means the corresponding absence of boundaries/boundary.

The -o (--output) flag allows you to specify the path and name of the output pointer to the fragment.

By default, a pointer is created to the fragment with the name of the original file and the extension ".fragpointer".

### fragplayer

`fragplayer <file (fragment pointer)>`

It is required to specify the path to the fragment pointer.
This command starts a text editor or media player to play the desired fragment.

### Examples:

`>: mediafragmenter /path/Concert.mkv -s 01:43:14 -f 01:47:38 -o song.fragpointer`

A pointer to the video fragment named 'song.fragpointer' will be created.

`>: mediafragmenter ~/Notes.txt -s 45,1 -f 59,31`

A pointer to the fragment named 'Notes.fragpointer' in the interval from the 45th line to the 31st character of the 59th line (inclusive) will be created.

`>: mediafragmenter /mnt/v/cartoon.avi -f 00:00:59 -o intro.fragpointer`

This command will create a pointer to the video from the begin of the video to the 59 seconds mark.

`>: fragplayer intro.fragpointer`

It uses tht pointer to the fragment created by the previous command. This command will launch media player (file 'cartoon.avi'), which will stop after the 59th second.

---

## Feedback

Questions, suggestions, comments, bugs:

**tech.vindex@gmail.com**

Also use the repository service tools.

---
