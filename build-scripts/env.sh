#!/bin/bash
PROJECT=mediafragmenter
APP1=mediafragmenter
APP2=fragplayer
ALL="changelog COPYING copyright Makefile README.md summary \
build-scripts/ help/ source/ res/"

readonly VERSION=`cat source/version`

if [[ -e /usr/bin/dpkg-architecture ]]; then
    DEB_HOST_MULTIARCH=`dpkg-architecture -qDEB_HOST_MULTIARCH`
    DISTR_IS_DEBIAN=TRUE
else
    DISTR_IS_DEBIAN=FALSE
fi

readonly BINPATH_BASEDIR=build/bin
BINPATH1="${BINPATH_BASEDIR}/${APP1}"
BINPATH2="${BINPATH_BASEDIR}/${APP2}"

