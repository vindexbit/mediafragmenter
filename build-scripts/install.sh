#!/bin/bash -e
source build-scripts/env.sh

MODE="$1"
PREFIX="$2"

if [[ ${PREFIX} != "" ]] ; then
    BASE="${PREFIX}"
else
    BASE="/usr/local"
fi

read -ra distr_id_arr <<< "`lsb_release -i`"
# lsb_release -i (for example): "Distributor ID: Ubuntu"
DISTRIBUTION=${distr_id_arr[2]}  # for example, "Ubuntu"

# RL is "Relative Location"
readonly RL_BINDIR=bin
readonly RL_SHAREDIR=share
readonly RL_HELPDIR=${RL_SHAREDIR}/help
# help directories for program №1
readonly RL_HELPDIR_EN_1=${RL_HELPDIR}/en_US/${APP1}
readonly RL_HELPDIR_RU_1=${RL_HELPDIR}/ru_RU/${APP1}
readonly RL_HELPDIR_EO_1=${RL_HELPDIR}/eo/${APP1}
# help directories for program №2
readonly RL_HELPDIR_EN_2=${RL_HELPDIR}/en_US/${APP2}
readonly RL_HELPDIR_RU_2=${RL_HELPDIR}/ru_RU/${APP2}
readonly RL_HELPDIR_EO_2=${RL_HELPDIR}/eo/${APP2}
# doc directory (for copyright file)
if [[ "${DISTRIBUTION}" != "openSUSE" ]]; then
    readonly RL_DOCDIR=${RL_SHAREDIR}/doc
else
    readonly RL_DOCDIR=${RL_SHAREDIR}/doc/packages
fi
# doc directory for this project
readonly RL_DOCDIR_SPEC=${RL_DOCDIR}/${PROJECT}
# directory for bash-completions
readonly RL_BASHCOMP=${RL_SHAREDIR}/bash-completion/completions

INST_BINDIR="${BASE}/${RL_BINDIR}"
INST_BASHCOMPDIR="${BASE}/${RL_BASHCOMP}"
INST_DOCDIR_SPEC="${BASE}/${RL_DOCDIR_SPEC}"
INST_SHARE_DIR="${BASE}/${RL_SHAREDIR}"

INST_HELPDIR_EN_1="${BASE}/${RL_HELPDIR_EN_1}"
INST_HELPDIR_RU_1="${BASE}/${RL_HELPDIR_RU_1}"
INST_HELPDIR_EO_1="${BASE}/${RL_HELPDIR_EO_1}"

INST_HELPDIR_EN_2="${BASE}/${RL_HELPDIR_EN_2}"
INST_HELPDIR_RU_2="${BASE}/${RL_HELPDIR_RU_2}"
INST_HELPDIR_EO_2="${BASE}/${RL_HELPDIR_EO_2}"

set -x

echo "PREFIX:" ${BASE}

if [[ ${MODE} == "--install" ]] ; then
    # prepare directories
    mkdir -p "${INST_BINDIR}" "${INST_BASHCOMPDIR}" "${INST_DOCDIR_SPEC}"
    # install executable file and bash completion script
    install "${BINPATH1}" "${INST_BINDIR}/"
    install "${BINPATH2}" "${INST_BINDIR}/"
    if [[ -e source/_${APP1} ]] ; then
        mkdir -p "${INST_BASHCOMPDIR}"
        cp source/_${APP1} "${INST_BASHCOMPDIR}/${APP1}"
    fi
    if [[ -e source/_${APP2} ]] ; then
        mkdir -p "${INST_BASHCOMPDIR}"
        cp source/_${APP2} "${INST_BASHCOMPDIR}/${APP2}"
    fi

    # copy copyright file
    cp copyright ${INST_DOCDIR_SPEC}/

    # install help files
    mkdir -p ${INST_HELPDIR_EN_1} ${INST_HELPDIR_RU_1} ${INST_HELPDIR_EO_1}
    cp help/${APP1}/help_en_US.txt ${INST_HELPDIR_EN_1}/help.txt
    cp help/${APP1}/help_ru_RU.txt ${INST_HELPDIR_RU_1}/help.txt
    cp help/${APP1}/help_eo.txt    ${INST_HELPDIR_EO_1}/help.txt
    mkdir -p ${INST_HELPDIR_EN_2} ${INST_HELPDIR_RU_2} ${INST_HELPDIR_EO_2}
    cp help/${APP2}/help_en_US.txt ${INST_HELPDIR_EN_2}/help.txt
    cp help/${APP2}/help_ru_RU.txt ${INST_HELPDIR_RU_2}/help.txt
    cp help/${APP2}/help_eo.txt    ${INST_HELPDIR_EO_2}/help.txt
    
    mkdir -p ${INST_SHARE_DIR}/mime/packages/ ${INST_SHARE_DIR}/applications/
    cp res/fragpointer.xml ${INST_SHARE_DIR}/mime/packages/
    cp res/fragplayer.desktop ${INST_SHARE_DIR}/applications/
    # xdg-mime default fragplayer.desktop text/fragpointer
    # update-mime-database ${INST_SHARE_DIR}/mime/

else
    rm -f ${INST_BINDIR}/${APP1} ${INST_BASHCOMPDIR}/${APP1}
    rm -f ${INST_BINDIR}/${APP2} ${INST_BASHCOMPDIR}/${APP2}

    rm -rf ${INST_HELPDIR_EN_1} ${INST_HELPDIR_RU_1} ${INST_HELPDIR_EO_1}
    rm -rf ${INST_HELPDIR_EN_2} ${INST_HELPDIR_RU_2} ${INST_HELPDIR_EO_2}

    rm -rf ${INST_DOCDIR_SPEC}
    
    rm ${INST_SHARE_DIR}/mime/packages/fragpointer.xml
    rm ${INST_SHARE_DIR}/applications/fragplayer.desktop
    update-mime-database ${INST_SHARE_DIR}/mime/
fi


set +x
