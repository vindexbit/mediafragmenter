#!/bin/bash -e
. build-scripts/env.sh

if [[ ! -e testing/ ]]; then
    echo "No tests"
    exit
fi

cp ${BINPATH} testing/
cd testing
  ./testing.sh
cd - > /dev/null
rm testing/${APP}
