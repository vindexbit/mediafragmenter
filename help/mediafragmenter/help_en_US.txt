mediafragmenter is a tool for creating pointers to fragments.
A pointer to a fragment is a file with a description of the file type,
the file path, the begin of the fragment in the file and the end of the
fragment of the file.
In this version, working with fragments of video, audio and plain text documents
(txt, xml, source code, etc.) is available.

To play the fragments use another program:
fragplayer --help

Usage:

mediafragmenter <file> [-t|--type <type>] [-s|--start <begin>] \
                       [-f|--finish <end>] [-o|--output <output file>]
    Only the path to the file is required, the pointer to the fragment of which
    is created; other parameters are optional.
    
    The -t (--type) flag allows you to specify the file type:
    video, audio or text.
    If the flag is not specified, the type is determined automatically.
    
    The -s (--start) and -f (--finish) flags specify
    the boundaries of the fragment:
    
        for audio and video types, the time stamps are used in the format
        HH:MM:SS (hours, minutes, seconds with a colons);
        seconds can have a fractional part up to four characters after point;
        
        for text, the boundaries are the row number and the column number
        (number of symbol in the row) separated by comma (without a space);
        the boundaries are closed, i.e. fragments
        includes specified boundaries;
        
        the absence of the value of the begin and/or end of the fragment
        means the corresponding absence of boundaries/boundary.
    
    The -o (--output) flag allows you to specify the path
    and name of the output pointer to the fragment.
    By default, a pointer is created to the fragment with the name of
    the original file and the extension ".fragpointer".
    
    Examples:
    
    mediafragmenter ~/Concert.mkv -s 01:43:14 -f 01:47:38 -o song.fragpointer
    A pointer to the video fragment named 'song.fragpointer' will be created.
    
    mediafragmenter ~/Notes.txt -s 45,1 -f 59,31
    A pointer to the fragment named 'Notes.fragpointer' in the interval from
    the 45th line to the 31st character of the 59th line (inclusive)
    will be created.
    
    mediafragmenter ~/cartoon.avi -f 00:00:59 -o intro.fragpointer
    This command will create a pointer to the video from the begin of the video
    to the 59 seconds mark.
    

mediafragmenter --version
    It displays the version of the application.

mediafragmenter --help
    It displays this help.

© Eugene 'Vindex' Stulin, 2019
